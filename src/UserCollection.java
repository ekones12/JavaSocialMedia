

public class UserCollection {
	/**
	 * This function for adding user list from user.txt
	 * @param command this commands for ADDUSER line 
	 */
	public static void adduser(String command[]){
		User.users.add(new User(command[1],command[2],command[3],command[4],command[5]));
		System.out.printf("%s has been successfully added.\n",command[1]);
	}
	
	/**
	 * This function for remove users from user list
	 * @param command this commands for REMOVEUSER line 
	 */
	public static void removeUser(String command[]){
		int b=-1;
		for(User find : User.users){
			if (Integer.parseInt(command[1])==find.userId){
				b = User.users.indexOf(find);
				}
			}
		if(b>=0){
			User.users.remove(b);
		System.out.println("User has been successfully removed.");
		}
		else{
			System.out.println("No such user!");
		}	
	}
	
	/**
	 * This function for sing in for application
	 * @param command this commands for SINGIN line 
	 */
	public static void userSignin(String command[]){
		int b=-1;
		for(User find : User.users){
			if (command[1].equals(find.username)){
				b=1;
				if(command[2].equals(find.password)){
					System.out.println("You have successfully signed in.");
					User.signed = 1;
					User.signedname = command[1];
				}
				else{
					System.out.println("Invalid username or password! Please try again.");
				}
			}
		}
		if(b<0)
			System.out.println("No such user!");
		
	
}
	/**
	 * This function for showing posts from post list 
	 * @param command this commands for SHOWPOST line 
	 */
	public static void showPosts(String command[]){
	
		
		System.out.printf("**************\n%s�s Posts\n**************\n",command[1]);
		for(Posts found : Posts.posts){
			
		if(found.whois.equals(command[1])){
			System.out.println(found.text + "\nDate: "+found.date+"\nLocation: "+ found.Locations);
			if(found.text.contains("image")){
				ImagePost founder = (ImagePost) found;
				System.out.println("Image: "+founder.imagefilename+"\nImage resolution: "+founder.imageresolution);
			}
			if(found.text.contains("video")){
				VideoPost founder = (VideoPost) found;
				System.out.println("Video: "+founder.videofilename+"\nVideo duration: "+founder.videoresolution);
			}
			if(found.taggedusers.length>0){
					if(found.taggedusers[0] == ""){
						}
					else{
					System.out.println("Friends tagged in this post: "+String.join(",",found.taggedusers));}
				
			}
			System.out.println("----------------------");
		}
		
	}
	
	
}
	
	
}
