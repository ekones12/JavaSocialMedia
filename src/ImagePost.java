
public class ImagePost  extends TextPost {
	
	 String imagefilename;
	 String imageresolution;
	 ImagePost(String text,String longitude,String latitude,String tagged,String imagefilename,String imageresolution){
		 super(text,longitude,latitude,tagged);
		 
		 this.imagefilename=imagefilename;
		 this.imageresolution=imageresolution;
		 
		 
	 }
	 /**
	  * this function for adding image posts
	  * @param command this is reading argument of command for adding image post
	  */
	 public static void addpostimg(String command[]){
			String asd="";
			 if(User.signed>0){
				 for(User find : User.users){ 
					 
					   if (User.signedname.equals(find.username)){  
						  Posts found = new ImagePost(command[1],command[2],command[3],command[4],command[5],command[6]);
			 					  for(int j=0;j<found.taggedusers.length;j++){
			 						 if(find.friends.contains(found.taggedusers[j])){
			 							asd+=found.taggedusers[j]+":";
			 							
			 						 }
			 						 else{
			 							 System.out.println(found.taggedusers[j]+" is not your friend, and will not be tagged!");
			 							
			 						 }
							  }
			 					 posts.add(new ImagePost(command[1],command[2],command[3],asd,command[5],command[6]));
			 					 System.out.println("The post has been successfully added.");
							  
						  }
					   }
				 }
				 
			 
			  else{
				   System.out.println("Error: Please sign in and try again.");
			   }
		}	
		
}
