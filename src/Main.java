import java.io.BufferedReader;
import java.util.UUID;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main (String [] args)// this is Main function for all operations
    {
    	User.usertxt(args[0]); 
        try{
            FileReader fileReader = new FileReader(args[1]);
            BufferedReader br = new BufferedReader(fileReader);
            String str;
            while ((str = br.readLine()) != null)
            {
                System.out.printf("-----------------------\nCommand: %s\n",str);
                String commandsplit[] = str.split("	");
                if(commandsplit[0].equals("ADDUSER")){
                    UserCollection.adduser(commandsplit);
                    
                }
                if(commandsplit[0].equals("REMOVEUSER")){
                    UserCollection.removeUser(commandsplit);
                    
                }
                if(commandsplit[0].equals("SIGNIN")){
                    UserCollection.userSignin(commandsplit);
                    
                }
                if(commandsplit[0].equals("SIGNOUT")){
                    System.out.println("You have successfully signed out.");
                    User.signed = -1;
                    User.signedname="";
                    
                }
                if(commandsplit[0].equals("ADDFRIEND")){
                    User.addfriend(commandsplit);
                    
                }
                if(commandsplit[0].equals("REMOVEFRIEND")){
                	User.removefriend(commandsplit);
                    
                }
                if(commandsplit[0].equals("BLOCK")){
                	User.blockuser(commandsplit);
                    
                }
                if(commandsplit[0].equals("UNBLOCK")){
                	User.unblockuser(commandsplit);
                    
                }
                if(commandsplit[0].equals("SHOWBLOCKEDFRIENDS")){
                	User.showblockedfriends(commandsplit);
                    
                }
                if(commandsplit[0].equals("SHOWBLOCKEDUSERS")){
                	User.showblockedusers(commandsplit);
                    
                }
                if(commandsplit[0].equals("LISTFRIENDS")){
                	User.listfriends(commandsplit);
                    
                }
                if(commandsplit[0].equals("CHPASS")){
                	User.chpass(commandsplit);
                    
                }
                if(commandsplit[0].equals("LISTUSERS")){
                	User.listusers(commandsplit);
                    
                }
                if(commandsplit[0].equals("UPDATEPROFILE")){
                	User.updateprofile(commandsplit);
                   
                }
                if(commandsplit[0].equals("ADDPOST-TEXT")){
                	TextPost.addpost(commandsplit);
                   
                }
                if(commandsplit[0].equals("ADDPOST-IMAGE")){
                	ImagePost.addpostimg(commandsplit);
                   
                }
                if(commandsplit[0].equals("ADDPOST-VIDEO")){
                	VideoPost.addpostvideo(commandsplit);
                   
                }
                if(commandsplit[0].equals("REMOVELASTPOST")){
                	Posts.deletelast(commandsplit);
                   
                }
                if(commandsplit[0].equals("SHOWPOSTS")){
                	UserCollection.showPosts(commandsplit);
                   
                }
            } 
            br.close();
        }catch(IOException e){

        }
    }
}
