import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class User{
	public static ArrayList<User> users = new ArrayList<User>();
   
	public static int signed=0;
    public static String signedname;
    String name;
    String username;
    String password;
    String graduatedSchool;
    String dateofBirth;
    String postId;
    ArrayList<String> friends = new ArrayList<String>();
    ArrayList<String> blockedusers = new ArrayList<String>();
    public int userId;
    private static int raisedinteger=1;
    User(String name,String username,String password,String dateofBirth,String graduatedSchool){
        this.name = name;
        this.username=username;
        this.password=password;
        this.dateofBirth= dateofBirth;
        this.graduatedSchool=graduatedSchool;
        this.userId = this.raisedinteger;
        this.raisedinteger++;
    }
    
    public static void readusers(String name,String username,String password,String dateofBirth,String graduatedSchool){
        users.add(new User(name,username,password,dateofBirth,graduatedSchool));
        }
    public static void usertxt(String args){
        try{
            FileReader fileReader = new FileReader(args);
            BufferedReader br = new BufferedReader(fileReader);
            String str;
            while ((str = br.readLine()) != null)
            {

             String ulist[] = str.split("\t");
             readusers(ulist[0],ulist[1],ulist[2],ulist[3],ulist[4]);
   

            }
            br.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    public static void listusers(String command[]){
    	if(signed>0){
 		   for(User find : users){

				  System.out.println("Name: "+ find.name+
						  			"\nUsername: " +find.username+
						  			"\nDate of Birth: "+find.dateofBirth+
						  			"\nSchool: "+find.graduatedSchool+
						  			"\n---------------------------");
			
 			   
 			   
 		   }
    	}
    	
    	
    	
    }
    
    
    public static void chpass(String command[]){

    	 if(signed>0){
    		  for(User find : users){ 
   			   if (signedname.equals(find.username)){
   				   if(find.password.equals(command[1])){
   					   find.password=command[2];   
   				   }
   				   else{
   					   System.out.println("Password mismatch! Please, try again.");
   					   }
   				   }
   			   }
    		  }
    	
    	 else{
  		   System.out.println("Error: Please sign in and try again.");
  	   	}
    }
 
	   
    public static void addfriend(String command[]){
	   ArrayList<String> already = new ArrayList<String>();
	   if(signed>0){
		   
		   for(User find : users){ 
			   already.add(find.name.toLowerCase());
			   }
		  
		   for(User find : users){ 
			   if (signedname.equals(find.username)){
				   if(find.friends.contains(command[1])){
				   System.out.println("This user is already in your friend list!");
				   }
				   else{
					   if(already.contains(command[1])){
					   find.friends.add(command[1]);
					   System.out.printf("%s has been successfully added to your friend list.\n",command[1]);
					   }else{
						   System.out.println("No such user!");
					   }
				   }
			   }
		   }
	   }
 
	   else{
		   System.out.println("Error: Please sign in and try again.");
	   }   
   }
    public static void blockuser(String command[]){
	   ArrayList<String> already = new ArrayList<String>();
	   if(signed>0){
		   
		   for(User find : users){ 
			   already.add(find.name.toLowerCase());
			   }
		  
		   for(User find : users){ 
			   if (signedname.equals(find.username)){
				   if(find.blockedusers.contains(command[1])){
				   System.out.println("This user is already blocked!");
				   }
				   else{
					   if(already.contains(command[1])){
					   find.blockedusers.add(command[1]);
					   System.out.printf("%s has been successfully blocked.\n",command[1]);
					   }else{
						   System.out.println("No such user!");
					   }
				   }
			   }
		   }
	   }
 
	   else{
		   System.out.println("Error: Please sign in and try again.");
	   }   
   }
    
    public static void removefriend(String command[]){
	   
	   if(signed>0){
		   for(User find : users){ 
			   if (signedname.equals(find.username)){
				   if(find.friends.contains(command[1])){
					   find.friends.remove(command[1]);
					   System.out.printf("%s has been successfully removed from your friend list.\n",command[1]);
					   }
				   else{
					   System.out.println("No such friend!");
				   }  
			   }			   
		   } 		   	  
	   }
	   
	   
	   else{
		   System.out.println("Error: Please sign in and try again.");
	   }
   }

    public static void unblockuser(String command[]){
	   
	   if(signed>0){
		   for(User find : users){ 
			   if (signedname.equals(find.username)){
				   if(find.blockedusers.contains(command[1])){
					   find.blockedusers.remove(command[1]);
					   System.out.printf("%s has been successfully unblocked.\n",command[1]);
					   }
				   else{
					   System.out.println("No such user in your blocked users list!");
				   }  
			   }			   
		   } 		   	  
	   }
	   
	   
	   else{
		   System.out.println("Error: Please sign in and try again.");
	   }
   }
  
    
    public static void showblockedfriends(String command[]){
    	
    	if(signed>0){
    		for(User find : users){
 			   if (signedname.equals(find.username)){
 				   if(find.blockedusers.size()>0){
 					  if(find.friends.size()>0){
 				  for (int i=0;i<find.blockedusers.size();i++){
 					  for(int j=0;j<find.friends.size();j++){
 						  if(find.blockedusers.get(i).equals(find.friends.get(j))){ 
 							  System.out.println("Name: "+ name(find.blockedusers.get(i))+
 									  			"\nUser Name: " +username(find.blockedusers.get(i))+
 									  			"\nDate of Birth: "+date(find.blockedusers.get(i))+
 									  			"\nSchool: "+school(find.blockedusers.get(i))+
 									  			"\n---------------------------");
 							  		}
 						  		}
 					  		}
 					  	}
 					  else{
 						 System.out.println("You haven�t blocked any friends yet!");
 					  }
 				  }
 				   else{
 					   System.out.println("You haven�t blocked any users yet!");
 				  }
 			   	}
 			  }
    		}
    	
    	else{
   		   System.out.println("Error: Please sign in and try again.");
   	   }   	
    }
   
    public static void showblockedusers(String command[]){
    	
    	if(signed>0){
    		for(User find : users){
    			if (signedname.equals(find.username)){
    				if(find.blockedusers.size()>0){
    					for (int i=0;i<find.blockedusers.size();i++){
    						System.out.println("Name: "+ name(find.blockedusers.get(i))+
									  			"\nUsername: " +username(find.blockedusers.get(i))+
									  			"\nDate of Birth: "+date(find.blockedusers.get(i))+
									  			"\nSchool: "+school(find.blockedusers.get(i))+
									  			"\n---------------------------");
		 							  }
		 				  }
	 				  else{System.out.println("You haven�t blocked any users yet!");
	 				  	}
    				}
    			}
    		}
    	else{
   		   System.out.println("Error: Please sign in and try again.");
   	   }  
    }   
    public static void updateprofile(String command[]){
    	
    	 if(signed>0){
   		  	for(User find : users){ 
   		  		if (signedname.equals(find.username)){
	   				find.name=command[1];
	   				find.dateofBirth=command[2];
	   				find.graduatedSchool=command[3];
	   				System.out.println("Your user profile has been successfully updated.");
	   				}
   		  		}
   		  	}
    	 else{
    		   System.out.println("Error: Please sign in and try again.");
    		   }
    	 }
    public static void listfriends(String command[]){    	
    	if(signed>0){
    		for(User find : users){
 			   if (signedname.equals(find.username)){
 				  for (int i=0;i<find.friends.size();i++){
 							  System.out.println("Name: "+ name(find.friends.get(i))+
 									  			"\nUsername: " +username(find.friends.get(i))+
 									  			"\nDate of Birth: "+date(find.friends.get(i))+
 									  			"\nSchool: "+school(find.friends.get(i))+
 									  			"\n---------------------------");	
 					  }
 				  }
 			   }
    		}
    	 else{
   		   System.out.println("Error: Please sign in and try again.");
   	   	}   	
    }
    public static String name(String a){
    	String name="";
    	for(User find : users){
			if (a.equals(find.username)){
				name=find.name;
				}
			}
    	return name ;
    }
    public static String username(String a){
    	String name="";
    	for(User find : users){
			if (a.equals(find.username)){
				name=find.username;
				}
			}
    	return name ;
    }
    public static String date(String a){
    	String name="";
    	for(User find : users){
			if (a.equals(find.username)){
				name=find.dateofBirth;
				}
			}
    	return name ;
    }
    public static String school(String a){
    	String name="";
    	for(User find : users){
			if (a.equals(find.username)){
				name=find.graduatedSchool;
				}
			}
    	return name ;
    }
    
    
}