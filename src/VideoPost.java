
public class VideoPost extends TextPost {
	
	
	String videofilename;
	String videoresolution;
	int maxvideotime=10;
	VideoPost(String text,String longitude,String latitude,String tagged,String videofilename,String videoresolution){
		 super(text,longitude,latitude,tagged);
		 this.videofilename=videofilename;
		 this.videoresolution=videoresolution;
	}
	 /**
	  * this function for adding video posts
	  * @param command this is reading argument of command for adding video post
	  */
	public static void addpostvideo(String command[]){
		String asd="";
		 if(User.signed>0){
			 for(User find : User.users){ 
				 
				   if (User.signedname.equals(find.username)){  
					  Posts found = new VideoPost(command[1],command[2],command[3],command[4],command[5],command[6]);
					  if(Integer.parseInt(command[6])<=10){
		 					  for(int j=0;j<found.taggedusers.length;j++){
		 						 if(find.friends.contains(found.taggedusers[j])){
		 							asd+=found.taggedusers[j]+":";
		 							
		 						 }
		 						 else{
		 							 System.out.println(found.taggedusers[j]+" is not your friend, and will not be tagged!");
		 							
		 						 }
						  }
		 					 posts.add(new VideoPost(command[1],command[2],command[3],asd,command[5],command[6]));
		 					 System.out.println("The post has been successfully added.");
					  }else{
						  System.out.println("Error: Your video exceeds maximum allowed duration of 10 minutes.");
					  }
					  }
				   }
			 }
			 
		 
		  else{
			   System.out.println("Error: Please sign in and try again.");
		   }
	}	
}
